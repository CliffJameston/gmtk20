﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
	[Tooltip("Game Controller")]
	[SerializeField]
	GameObject gameController = null;

	// Start is called before the first frame update
	void Start()
	{

	}

	private void OnTriggerEnter(Collider other)
	{
		if (gameController.GetComponent<GameController>().debugMode == true)
		{
			Debug.Log("Collision ocurred with " + other.name);
			return;
		}
		if (other.CompareTag("Obstacle"))
		{
			gameController.SendMessage("PlayerLost");
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (gameController.GetComponent<GameController>().debugMode == true)
		{
			Debug.Log("Collision ocurred with " + collision.gameObject.name);
			return;
		}
		if (collision.gameObject.CompareTag("Obstacle"))
		{
			gameController.SendMessage("PlayerLost");
		}
	}

	// Update is called once per frame
	void Update()
	{
		
	}
}
