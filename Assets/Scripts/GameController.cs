﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	[Tooltip("Disable gameplay features?")]
	[SerializeField]
	public bool debugMode = false;

	[Tooltip("Game Title UI Element")]
	[SerializeField]
	GameObject titleText = null;
	[Tooltip("Play Button UI Element")]
	[SerializeField]
	GameObject playButton = null;
	[Tooltip("Menu Screen")]
	[SerializeField]
	GameObject menuScreen = null;
	[Tooltip("Game Screen")]
	[SerializeField]
	GameObject gameScreen = null;

	[Tooltip("Speedometer UI Element")]
	[SerializeField]
	GameObject speedText = null;
	[Tooltip("Fuel Meter UI Element")]
	[SerializeField]
	GameObject fuelText = null;
	[Tooltip("Score UI Element")]
	[SerializeField]
	GameObject scoreText = null;

	[Tooltip("The Player object")]
	[SerializeField]
	GameObject player = null;

	public enum gameState { MainMenu, Playing, Loss, Win}
	public gameState currentState;
	private void Awake()
	{

	}

	// Start is called before the first frame update
	void Start()
	{
		if (debugMode == true)
		{
			StartGame();
		}
		else
		{
			currentState = gameState.MainMenu;
			player.SetActive(false);
			ShowMenu();
		}
		
	}

	// Update is called once per frame
	void Update()
	{
		if (currentState == gameState.Playing)
		{
			UpdateUI();
		}
	}
	private void ShowMenu()
	{
		switch (currentState)
		{
			case gameState.MainMenu:
				titleText.GetComponent<Text>().text = "Project Speed";
				playButton.GetComponentInChildren<Text>().text = "Play";
				menuScreen.SetActive(true);
				gameScreen.SetActive(false);
				break;
			case gameState.Playing:
				break;
			case gameState.Loss:
				titleText.GetComponent<Text>().text = "You Crashed!";
				playButton.GetComponentInChildren<Text>().text = "Play Again";
				menuScreen.SetActive(true);
				gameScreen.SetActive(true);
				break;
			case gameState.Win:
				titleText.GetComponent<Text>().text = "You Won!";
				playButton.GetComponentInChildren<Text>().text = "Play Again";
				menuScreen.SetActive(true);
				gameScreen.SetActive(false);
				break;
			default:
				break;
		}
	}

	private void UpdateUI()
	{
		string fuelReading = player.GetComponent<MovementController>().GetFuel();
		fuelText.GetComponent<Text>().text = "Fuel: " + fuelReading;
		string speedReading = player.GetComponent<MovementController>().GetSpeed();
		speedText.GetComponent<Text>().text = "Speed: " + speedReading;
		string scoreReading = player.GetComponent<MovementController>().GetScore();
		scoreText.GetComponent<Text>().text = "Score: " + scoreReading;
	}

	public void StartGame()
	{
		menuScreen.SetActive(false);
		gameScreen.SetActive(true);
		player.SetActive(true);
		currentState = gameState.Playing;
	}

	public void QuitGame()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void PlayerLost()
	{
		player.SetActive(false);
		currentState = gameState.Loss;
		ShowMenu();
	}

	public void PlayerWon()
	{
		player.SetActive(false);
		currentState = gameState.Win;
		ShowMenu();
	}
}
