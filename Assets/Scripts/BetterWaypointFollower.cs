using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityStandardAssets.Utility;

#if UNITY_EDITOR
using UnityEditor;
#endif

/**
 * Created by Zaneris.
 * Follows the route of a WaypointCircuit at a constant speed.
 * Enables Waypoint based speed settings.
 * Enables invoke of method calls triggered by waypoints.
 **/
public class BetterWaypointFollower : MonoBehaviour
{
	public UnityEngine.Object circuitObject;
	public WaypointCircuit circuit;
	public float routeSpeed = 50f;
	public float lookAheadDistance = 100f;

	private float[] distances;
	private float progressDistance;
	private float currentSpeed;
	private int lastWaypoint;


	private void Start()
	{
		// Set up our distances, so we know which waypoint we're at.
		distances = new float[circuit.Waypoints.Length + 1];
		for(int i = 1; i < circuit.Waypoints.Length; i++)
			distances[i] = (circuit.Waypoints[i].position - circuit.Waypoints[i - 1].position).magnitude + distances[i - 1];
		distances[circuit.Waypoints.Length] = (circuit.Waypoints[circuit.Waypoints.Length - 1].position - circuit.Waypoints[0].position).magnitude + distances[circuit.Waypoints.Length - 1];

		ResetWaypointCircuit();
	}

	// Reset everything to the starting point.
	public void ResetWaypointCircuit()
	{
		progressDistance = 0;
		transform.position = circuit.Waypoints[0].position;
		transform.LookAt(circuit.GetRoutePoint(lookAheadDistance + 6.576f).position);
		currentSpeed = routeSpeed;
	}

	public void Accelerate(float accelerationSpeed)
	{
		currentSpeed = currentSpeed + (accelerationSpeed * Time.deltaTime);
	}

	public float GetCurrentSpeed()
	{
		return currentSpeed;
	}

	private void Update()
	{
		#region Determining our position relative to waypoints.
		if(progressDistance > distances[distances.Length - 1])
			progressDistance -= distances[distances.Length - 1];
		for(int i = 1; i < distances.Length; i++)
			if(progressDistance < distances[i])
			{
				// Check if we've passed a new waypoint.
				lastWaypoint = i - 1;
				break;
			}
		#endregion
		//currentSpeed = routeSpeed;

		Vector3 nextPosition, nextDelta;
		do
		{ // Making sure our target point is far enough ahead on the route.
			progressDistance += Time.deltaTime * currentSpeed * .8f;
			nextPosition = circuit.GetRoutePoint(progressDistance).position;
			nextDelta = nextPosition - transform.position;
		} while(nextDelta.magnitude < 10f);

		nextDelta.Normalize(); // Set our direction vector to exactly 1 in magnitude.
		nextDelta *= currentSpeed; // Scale it back up to exactly our specified speed.
		transform.position += nextDelta * Time.deltaTime;
		transform.LookAt(circuit.GetRoutePoint(progressDistance + lookAheadDistance).position);
	}

	private void OnDrawGizmos()
	{
		if(Application.isPlaying)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawLine(transform.position, transform.position);
			Gizmos.DrawWireSphere(circuit.GetRoutePosition(progressDistance), 1);
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(transform.position, transform.position + transform.forward);
		}
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(BetterWaypointFollower))]
public class BetterWaypointEditor : Editor
{
	List<String> monoBehaviours;
	int totalMethods;

	public override void OnInspectorGUI()
	{
		BetterWaypointFollower script = (BetterWaypointFollower)target;
		script.circuitObject = EditorGUILayout.ObjectField("Waypoint Circuit", script.circuitObject, typeof(WaypointCircuit), true);
		if(script.circuitObject != null)
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("-- Settings --", EditorStyles.boldLabel);
			script.circuit = (WaypointCircuit)script.circuitObject;
			script.routeSpeed = EditorGUILayout.FloatField("Speed In Units/Sec", script.routeSpeed);
			script.lookAheadDistance = EditorGUILayout.FloatField("Look Ahead Distance", script.lookAheadDistance);
		}
	}
}
#endif
