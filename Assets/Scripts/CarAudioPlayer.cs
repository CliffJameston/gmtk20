﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAudioPlayer : MonoBehaviour
{
    [SerializeField]
    GameObject gameController = null;
    [SerializeField]
    GameObject playerCamera = null;

    public AudioClip highAccelClip;                                             // Audio clip for high acceleration
    public float pitchMultiplier = 1f;                                          // Used for altering the pitch of audio clips
    public float lowPitchMin = 1f;                                              // The lowest possible pitch for the low sounds
    public float lowPitchMax = 6f;                                              // The highest possible pitch for the low sounds
    public float highPitchMultiplier = 0.25f;                                   // Used for altering the pitch of high sounds
    public float maxRolloffDistance = 500;                                      // The maximum distance where rollof starts to take place
    public float dopplerLevel = 1;                                              // The mount of doppler effect used in the audio
    public bool useDoppler = true;                                              // Toggle for using doppler

    private AudioSource m_HighAccel; // Source for the high acceleration sounds
    private bool m_StartedSound; // flag for knowing if we have started sounds
    private BetterWaypointFollower cameraFollower;
    private GameController ourController;

    private void Start()
    {
        ourController = gameController.GetComponent<GameController>();
        cameraFollower = playerCamera.GetComponent<BetterWaypointFollower>();
    }

    private void StartSound()
    {
        // setup the simple audio source
        m_HighAccel = SetUpEngineAudioSource(highAccelClip);

        // flag that we have started the sounds playing
        m_StartedSound = true;
    }


    private void StopSound()
    {
        //Destroy all audio sources on this object:
        foreach (var source in GetComponents<AudioSource>())
        {
            Destroy(source);
        }

        m_StartedSound = false;
    }


    // Update is called once per frame
    private void Update()
    {
        // stop sound if the object is beyond the maximum roll off distance
        if (m_StartedSound && ourController.currentState != GameController.gameState.Playing)
        {
            StopSound();
        }

        // start the sound if not playing and it is nearer than the maximum distance
        if (!m_StartedSound && ourController.currentState == GameController.gameState.Playing)
        {
            StartSound();
        }

        if (m_StartedSound)
        {
            // The pitch is interpolated between the min and max values, according to the car's revs.
            float pitch = ULerp(lowPitchMin, lowPitchMax, cameraFollower.GetCurrentSpeed());

            // clamp to minimum pitch (note, not clamped to max for high revs while burning out)
            pitch = Mathf.Min(lowPitchMax, pitch);

            m_HighAccel.pitch = pitch * pitchMultiplier * highPitchMultiplier;
            m_HighAccel.dopplerLevel = useDoppler ? dopplerLevel : 0;
            m_HighAccel.volume = 1;
        }
    }


    // sets up and adds new audio source to the gane object
    private AudioSource SetUpEngineAudioSource(AudioClip clip)
    {
        // create the new audio source component on the game object and set up its properties
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = 0;
        source.loop = true;

        // start the clip from a random point
        source.time = Random.Range(0f, clip.length);
        source.Play();
        source.minDistance = 5;
        source.maxDistance = maxRolloffDistance;
        source.dopplerLevel = 0;
        return source;
    }


    // unclamped versions of Lerp and Inverse Lerp, to allow value to exceed the from-to range
    private static float ULerp(float from, float to, float value)
    {
        return (1.0f - value) * from + value * to;
    }
}