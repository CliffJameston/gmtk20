﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class MovementController : MonoBehaviour
{
	#region Serialized variables
	[Tooltip("Game Controller")]
	[SerializeField]
	GameObject gameController = null;
	[Tooltip("Camera")]
	[SerializeField]
	GameObject playerCamera = null;

	[Tooltip("Rate of Acceleration")]
	[SerializeField]
	float accelerationRate = 1f;
	[Tooltip("Rate of Deceleration")]
	[SerializeField]
	float decelerationRate = -5f;

	[Tooltip("In Meters per Second")]
	[SerializeField]
	float xSpeed = 15f;
	[Tooltip("Range to Clamp to")]
	[SerializeField]
	float xRange = 15f;

	[Tooltip("How much fuel should we start with?")]
	[SerializeField]
	float maxFuel = 150f;
	[Tooltip("How much fuel should we burn per second?")]
	[SerializeField]
	float burnRate = 1f;

	[Tooltip("How far should we be able to jerk?")]
	[SerializeField]
	float maxJerk = 3f;
	[Tooltip("How fast will we jerk?")]
	[SerializeField]
	float jerkSpeed = 30f;
	[Tooltip("How often should we be able to jerk?")]
	[SerializeField]
	float jerkIncrement = 3f;
	[Tooltip("What's the probability that we jerk this frame?")]
	[Range(0, 1)]
	[SerializeField]
	float jerkChance = 0.5f;
	#endregion

	float timeSinceLastJerk = 0f;
	float jerkRemaining = 0f;

	float currentFuel = 150f;
	float currentSpeed = 0f;
	float currentScore = 0f;

	BetterWaypointFollower cameraFollower;
	GameController ourController;

	private void Awake()
	{
		cameraFollower = playerCamera.GetComponent<BetterWaypointFollower>();
		ourController = gameController.GetComponent<GameController>();
	}

	// Start is called before the first frame update
	void Start()
	{
		currentFuel = maxFuel;
	}

	private void OnEnable()
	{
		PrepareGame();
	}

	// Update is called once per frame
	void Update()
	{
		switch (ourController.currentState)
		{
			case GameController.gameState.MainMenu:
				break;
			case GameController.gameState.Playing:
				if (ourController.debugMode == true)
				{
					ProcessMovement();
					//Accelerate();
					break;
				}
				// If there's still fuel in the tank, keep running
				if (currentFuel >= Mathf.Epsilon)
				{
					ProcessMovement();
					ProcessFuel();
					Accelerate();
				}
				else
				{
					// TODO: When player runs out of fuel, slow them down gradually, then when speed = 0, they win
					// You win!
					//while (currentSpeed >= Mathf.Epsilon)
					//{
					//	ProcessMovement();
					//	Decelerate();
					//}
					gameController.SendMessage("PlayerWon");
				}
				break;
			case GameController.gameState.Loss:
				break;
			case GameController.gameState.Win:
				break;
			default:
				break;
		}
	}

	public void PrepareGame()
	{
		ResetFuel();
		ResetSpeed();
	}

	public void ResetSpeed()
	{
		// TODO: Figure out why this freezes the game if called in PrepareGame()
		cameraFollower.ResetWaypointCircuit();
		//playerCamera.GetComponent<BetterWaypointFollower>().routeSpeed = startingSpeed;
	}

	public void ResetFuel()
	{
		currentFuel = maxFuel;
	}

	public string GetSpeed()
	{
		currentSpeed = cameraFollower.GetCurrentSpeed();

		if (currentSpeed <= Mathf.Epsilon)
		{
			return "0.0";
		}
		else
		{
			return currentSpeed.ToString("0.0");
		}
	}

	public string GetFuel()
	{
		if (currentFuel <= Mathf.Epsilon)
		{
			return "0.0";
		}
		else
		{
			return currentFuel.ToString("0.0");
		}
	}
	
	public string GetScore()
	{
		//currentSpeed = cameraFollower.GetCurrentSpeed();

		currentScore = ((maxFuel - currentFuel) * currentSpeed) * 100;

		if (currentScore <= Mathf.Epsilon)
		{
			return "0";
		}
		else
		{
			return currentScore.ToString("N0");
		}
	}

	private void Accelerate()
	{
		cameraFollower.Accelerate(accelerationRate);
	}

	private void Decelerate()
	{
		cameraFollower.Accelerate(decelerationRate);
	}

	private void ProcessFuel()
	{
		currentFuel = currentFuel - (burnRate * Time.deltaTime);
	}

	private void ProcessMovement()
	{
		float xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
		float xOffset = xThrow * xSpeed * Time.deltaTime;
		Jerk();
		float rawNewXPosition = Mathf.Clamp(transform.localPosition.x + xOffset + JerkOffset(), -xRange, xRange);

		transform.localPosition = new Vector3(rawNewXPosition, transform.localPosition.y, transform.localPosition.z);
	}

	private bool ShouldCarJerk()
	{
		// Calculate if enough time has passed to start considering jerking.
		// If not, add the amount of time that's passed since the last check.
		if (timeSinceLastJerk >= jerkIncrement)
		{
			return true;
		}
		timeSinceLastJerk += Time.deltaTime;
		return false;
	}

	private void Jerk()
	{
		// Sets the distance the car should jerk this time, then resets timeSinceLastJerk

		// If we *can* jerk,
		if (ShouldCarJerk() == true)
		{
			// Randomly determine if we should jerk or not
			// TODO: Make this more random. Maybe make the jerk chance matter more somehow (Since this runs every frame)
			if (UnityEngine.Random.Range(0f,1f) <= jerkChance)
			{
				// Then randomly determine how far to jerk, and reset timer
				jerkRemaining = UnityEngine.Random.Range(-maxJerk, maxJerk);
				timeSinceLastJerk = 0f;
			}
		}
	}

	private float JerkOffset()
	{
		float jerkOffset = jerkRemaining * jerkSpeed * Time.deltaTime;
		jerkRemaining = jerkRemaining - jerkOffset;
		return jerkOffset;
	}
}
